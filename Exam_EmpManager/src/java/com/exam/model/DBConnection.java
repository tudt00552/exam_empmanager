/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.model;

import com.exam.entity.Employee;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Setup
 */
public class DBConnection {
    public Connection conn;
    public Connection getConnection() throws SQLException {
        conn = DriverManager.getConnection("jdbc:derby://localhost:1527/Exam;user=sa;password=sa");
        return conn;
    }
    
    public void addEmployee(Employee emp) throws SQLException {
        PreparedStatement pstm = 
                getConnection().prepareStatement("INSERT INTO Employee (FULLNAME, BIRTHDAY, ADDRESS, POSITION, DEPARTMENT) VALUES (?, ?, ?, ?, ?)");
        pstm.setString(1, emp.getFullName());
        pstm.setString(2, emp.getBirthDay());
        pstm.setString(3, emp.getAddress());
        pstm.setString(4, emp.getPosition());
        pstm.setString(5, emp.getDepartment());
        pstm.executeUpdate();
    }
    
    public List<Employee> getEMP() throws SQLException{
        List<Employee> emplist = new ArrayList<>();
        PreparedStatement pstm = 
                getConnection().prepareStatement("SELECT * FROM Employee");
        ResultSet rs = pstm.executeQuery();
        while (rs.next()) {
            emplist.add(new Employee(rs.getInt("EMPID"), rs.getString("FULLNAME"), rs.getString("BIRTHDAY"), rs.getString("ADDRESS"), rs.getString("POSITION"), rs.getString("DEPARTMENT")));
          }
            return emplist;
    } 
}
