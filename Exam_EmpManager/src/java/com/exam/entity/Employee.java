/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.entity;

/**
 *
 * @author Setup
 */
public class Employee {
    public int EmpID;
    public String FullName;
    public String BirthDay;
    public String Address;
    public String Position;
    public String Department;

    public Employee() {
    }

    public int getEmpID() {
        return EmpID;
    }

    public void setEmpID(int EmpID) {
        this.EmpID = EmpID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public String getBirthDay() {
        return BirthDay;
    }

    public void setBirthDay(String BirthDay) {
        this.BirthDay = BirthDay;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String Position) {
        this.Position = Position;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String Department) {
        this.Department = Department;
    }

    public Employee(int EmpID, String FullName, String BirthDay, String Address, String Position, String Department) {
        this.EmpID = EmpID;
        this.FullName = FullName;
        this.BirthDay = BirthDay;
        this.Address = Address;
        this.Position = Position;
        this.Department = Department;
    }

}
