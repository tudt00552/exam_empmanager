<%-- 
    Document   : List
    Created on : Jun 11, 2019, 2:03:23 PM
    Author     : Setup
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <center>
            <h1>List Employee</h1>
            <table>
                <tr>
                    <th>EmployeeID</th>
                    <th>Full Name</th>
                    <th>BirthDay</th>
                    <th>Address</th>
                    <th>Position</th>
                    <th>Department</th>
                </tr>
                <c:forEach var="emp" items="${list}">
                    <tr>
                        <td>${emp.getEmpID()}</td>
                        <td>${emp.getFullName()}</td>
                        <td>${emp.getBirthDay()}</td>
                        <td>${emp.getAddress()}</td>
                        <td>${emp.getPosition()}</td>
                        <td>${emp.getDepartment()}</td>
                    </tr>
                </c:forEach>
            </table>
        </center>
    </body>
</html>
