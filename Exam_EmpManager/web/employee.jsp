<%-- 
    Document   : Employee
    Created on : Jun 11, 2019, 2:03:15 PM
    Author     : Setup
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            .form-group {
                display: inherit;
                margin-top: 10px;
            }
            
            .form-group label {
                text-align: left;
                padding-left:26px;
                width:125px;
                text-transform: uppercase;
                display:inline-block
            }
            
            .form-group input {
                padding-left:26px;
                width:150px;
                display:inline-block
            }
            
            button {
                background-color: #fff;
                border: 1px solid #333;
                width: 100px;
            }
        </style>
    </head>
    <body>
        <center>
            <h1>Hello World!</h1>
            <form action="EmployeeController" method="POST">
                <div class="form-group">
                    <label>Full Name: </label><input type="text" name="name"/>
                </div>
                <div class="form-group">
                    <label>Birthday: </label><input type="text" name="date" placeholder="dd/MM/yyyy"/>
                </div>
                <div class="form-group">
                    <label>Address: </label><input type="text" name="address"/>
                </div>
                <div class="form-group">
                    <label>Position: </label><input type="text" name="position"/>
                </div>
                <div class="form-group">
                    <label>Department: </label><input type="text" name="department"/>
                </div>
                <div class="form-group">
                    <button type="submit">Submit</button>
                    <button type="reset">Reset</button>
                </div>
            </form>
        </center>
    </body>
</html>
